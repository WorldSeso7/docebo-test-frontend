function Accordion(options)
{
	// validations on provided options
	if (!options) {
		console.error('options not provided');
		return;
	}

	// setting options to object instance
	this.options = options;

	// validation on container property
	if (!options.container) {
		console.error('container property not provided');
		return;
	}

	// getting and validating dom container element
	this.containerElement = document.getElementById(options.container);
	if (!this.containerElement) {
		console.error('container element not found');
		return;
	}

	// setting a class to the container to describe that accordion was initialized
	this.containerElement.classList.add('accordion');

	// define variable to keep the markup to be injected inside the container
	var containerElementInnerHTML = '';

	// getting markup for title
	var mainTitle = options.mainTitle;
	if (mainTitle) {
		containerElementInnerHTML += this.getMainTitleMarkup();
        this.containerElement.classList.add('accordion--with-main-title');
	}

	// getting markup for panels
	var panels = options.panels;
	if (panels && panels.length) {
		containerElementInnerHTML += this.getPanelsMarkup();
	}

	// setting generated markup
	this.containerElement.innerHTML = containerElementInnerHTML;

	// attaching events handling
	this.attachEvents();
}

Accordion.prototype.getMainTitleMarkup = function() {
	if (!this.options || !this.options.mainTitle) {
		return '';
	}
	return '' +
			'<div class="accordion__main-title">' + 
	            this.options.mainTitle +
	        '</div>';
}

Accordion.prototype.getPanelsMarkup = function() {
	if (!this.options || !this.options.panels || !this.options.panels.length) {
		return '';
	}

	var markup = '<div class="accordion__items">';

	for (var i in this.options.panels) {
		if (this.options.panels.hasOwnProperty(i)) {
			var currentPanel = this.options.panels[i];
			if (currentPanel) {
				markup += '' +
					'<div class="' + this.getPanelItemClass(currentPanel) + '">' +
		                '<div class="' + this.getPanelHeadClass(currentPanel) + '">' +
		                    '<div class="accordion__item-head-infos">' +
		                    	this.getPanelTitle(currentPanel) +
		                        this.getPanelSubtitle(currentPanel) +
		                    '</div>' +
		                    '<div class="accordion__item-head-arrow">' +
		                        '<i class="material-icons">' +
		                            'keyboard_arrow_down' +
		                        '</i>' +
		                    '</div>' +
		                '</div>' +
		                this.getPanelContent(currentPanel) +
		            '</div>';
			}
		} 
	}

	markup += '</div>';

	return markup;
};

Accordion.prototype.getPanelItemClass = function(panel) {
	if (!panel) {
		return 'accordion__item';
	}
	if (panel.open) {
		return 'accordion__item accordion__item--open';
	}
	else {
		return 'accordion__item accordion__item--closed';
	}
}

Accordion.prototype.getPanelHeadClass = function(panel) {
	if (!panel || !panel.subtitle) {
		return 'accordion__item-head';
	}
	return 'accordion__item-head accordion__item-head--with-subtitle';
}

Accordion.prototype.getPanelTitle = function(panel) {
	if (!panel || !panel.title) {
		return '';
	}
	return '' +
		'<div class="accordion__item-title">' +
            panel.title +
        '</div>';
}

Accordion.prototype.getPanelSubtitle = function(panel) {
	if (!panel || !panel.subtitle) {
		return '';
	}
	return '' +
		'<div class="accordion__item-subtitle">' +
            panel.subtitle +
        '</div>';
}

Accordion.prototype.getPanelContent = function(panel) {
	if (!panel || !panel.content) {
		return '';
	}
	return '' + 
		'<div class="accordion__item-body-wrapper">' +
			'<div class="accordion__item-body">' +
		        panel.content +
		    '</div>' +
	    '</div>';
}

Accordion.prototype.attachEvents = function() {
	if (this.containerElement) {
		this.containerElement.addEventListener('click', function (event) {
			var closestAccordionItem = event.target.closest('.accordion__item');
			if (closestAccordionItem) {
				closestAccordionItem.classList.toggle('accordion__item--open');
				closestAccordionItem.classList.toggle('accordion__item--closed');
			}
		}, false);
	}
}